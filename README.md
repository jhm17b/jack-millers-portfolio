# My Coursework Portfolio
*********************************************************************************************************************************************
## :space_invader: Jack H. Miller :space_invader:
I am currently an IT Senior graduating at Florida State University in Spring 2022, pursuing my technology related passions through a Bachelor's degree in Information Technology while fighting an aggressive form of Crohn's disease.
This passion for technology and knowledge being so great that even immediately coming to from a colostomy surgery cannot prevent me from taking summer finals to further my degree and the pursuit of growing my technological based skills and interests.
However, outside of academics you can find me enjoying some of my hobbies, which include things like evening runs, camping trips with friends, collecting the vinyl's for my favorite albums, or just relaxing after a long day with some video games or a nice book.
**********************************************************************************************************************************************
# Related Coursework:


### :floppy_disk: MySQL Coursework Portfolio: :floppy_disk:
### [MySQLPortfolio readme.md](https://bitbucket.org/jhm17b/mysql-portfolio/src/master/)

**********************************************************************************************************************************************

### :iphone: LIS4381 Mobile Web App Development: :iphone:
### [LIS4381 readme.md](https://bitbucket.org/jhm17b/lis4381/src/master/)
#### Course Description: 
This course focuses on concepts and best practices for managing mobile first projects. It goes over various processes and requirements for developing mobile applications and principles for effective interface and user experience design. Students develop a prototype of a mobile app and prepare a proposal and other documentation for communicating contractual and functional specifications to clients. Students also examine different issues and concerns that may influence the wide-spread adoption and implementation of mobile applications.

*********************************************************************************************************************************************************************************

### :hammer: LIS3781 Advanced Database (Under Construction): :hammer:
### [LIS3781 readme.md](https://bitbucket.org/jhm17b/lis3781/src/master/)
#### Course Description:
This course explores various topics in database management systems (DBMS), using a typical 
commercial DMBS (e.g., MySQL, SQL Server, Oracle), as well as non-relational data repositories 
(aka “NoSQL” databases). Administration, Security, stored procedures, triggers, transactions, 
functions, data mining, data analytics, data warehousing, and remote access to databases are some of 
the topics covered. The student is expected to demonstrate an understanding of these database concepts 
through creating, deploying, and utilizing various relational database designs. 

#### Course Objectives:
The goal of this course is to provide students with an advanced understanding of database design, 
implementation, and management concepts and techniques. Upon completion of the course, the student 
will be able to: 
* Perform advanced relational data modeling, using Entity-Relationship diagrams. 
* Design a data warehouse. 
* Apply performance tuning techniques 
* Administer and perform backup and recovery procedures. 
* Justify and define user’s roles, permissions, and access-levels as a database administrator. 
* Create database security settings. 
* Demonstrate an understanding of data warehousing, data mining, and data analytic techniques. 

*********************************************************************************************************************************************************************************

### :hammer: LIS4368 Advanced Web Applications Development (Under Construction): :hammer:
### [LIS4368 readme.md](https://bitbucket.org/jhm17b/lis4368/src/master/)
#### Course Description:
This course provides a foundation in developing web applications with an emphasis on server-side 
concepts, tools and methods. Topics include basic web application programming, advanced object-
oriented (OOP) and web application development. 
Students enrolled in this course will develop basic programming skills in a modern web development 
environment, understand web application development principles and be able to find and use web 
application development resources on the Internet. 

#### Course Objectives:
Upon successful completion of the course, the student should be able to: 
* Demonstrate the ability to program and deploy client/server-side scripts 
* Describe web-based input and output processes 
* Demonstrate web application to data source connectivity 
* Develop a dynamic web application using a mix of front-end and back-end web technologies 
* Employ OOP techniques, as well as business logic using a strongly typed language. 
* Create client- and server-side data validation. 